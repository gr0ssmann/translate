# translate

Auto-translate human-readable parts in code to English. Keeps a memory of what has already been translated, to translate files only if necessary.

Use it as follows:

1. Install the `openai` module: `pip install -U openai`
2. Put your API key into `api_key.txt`
3. Put your files into `input/` (some sample files are in there)
4. Run `python translate.py`
5. Inspect `output/`

*If necessary* change `prompt.txt`.

*If necessary*  change the model in line 26 of `translate.py`.

## License

Unless otherwise noted, everything in this repository is licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/), or (at your option) any later version.
