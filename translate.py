import hashlib
import json
import os, os.path
import re
import sys

from openai import OpenAI

USAGE = [0, 0]

with open("prompt.txt") as f:
    PROMPT = f.read().strip()

with open("api_key.txt") as k:
    client = OpenAI(api_key=k.read().strip())


def sha256(input_string):
    encoded_string = input_string.encode()
    sha256_hash = hashlib.sha256()
    sha256_hash.update(encoded_string)

    return sha256_hash.hexdigest()


def translate_text(text):
    response = client.chat.completions.create(
        model="gpt-4-turbo",
        messages=[
            {
                "role": "system",
                "content": PROMPT,
            },
            {"role": "user", "content": text},
        ],
        temperature=0,
    )

    USAGE[0] += response.usage.prompt_tokens
    USAGE[1] += response.usage.completion_tokens

    if response.choices[0].finish_reason != "stop":
        raise ValueError

    return response.choices[0].message.content.strip()


def clean(keep):
    for filename in os.listdir("output"):
        if (p := f"./output/{filename}") not in keep:
            os.remove(p)


def main():
    keep_outfiles = []

    try:
        os.mkdir("./output")
    except FileExistsError:
        pass

    try:
        with open("manifest.json") as j:
            manifest = json.load(j)
    except FileNotFoundError:
        manifest = {}

    for filename in os.listdir("input"):
        print(f"Translating {filename}…", end=" ", file=sys.stderr, flush=True)

        inpath = f"./input/{filename}"
        outpath = f"./output/{filename}"

        has_newline = False

        with open(inpath) as file:
            content = file.read()

            if content[-1] == "\n":
                has_newline = True

        if (
            (h := sha256(PROMPT + content)) in manifest
            and manifest[h] == outpath
            and os.path.isfile(manifest[h])
        ):
            print("skip.", file=sys.stderr, flush=True)
        else:
            try:
                out_content = translate_text(content.strip())
            except ValueError:
                print("error!", file=sys.stderr, flush=True)
                continue

            if has_newline and out_content[-1] != "\n":
                out_content += "\n"

            with open(outpath, "w") as f:
                f.write(out_content)

            manifest[h] = outpath

            if out_content == content:
                print("OK (no translation necessary).", file=sys.stderr, flush=True)
            else:
                print("OK.", file=sys.stderr, flush=True)

        keep_outfiles.append(outpath)

    try:
        with open("manifest.json", "w") as j:
            json.dump(manifest, j)
    except Exception as e:
        pass

    clean(keep_outfiles)

    print(
        f"\nDone.\nPrompt tokens: {USAGE[0]}\nCompletion tokes: {USAGE[1]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
